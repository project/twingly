
-- SUMMARY --

Twingly.com is a search engine for blogs and microblogs. With this module you 
can create new Twingly Widgets as blocks. You will also be able to ping Twingly 
with your new blog content. 

With the Twingly module you can create blocks that...
=====================================================
 - List searchresult for a certain searchphrase from the blogosphere.
 - List searchresult for a certain searchphrase from microblogs such as Twitter.
 - List other Blogsposts that has linked to yours. 
 
 * You can list from 1 to 10 results. 
 * You can order by date, inlinks or TwinglyRank. 
 * You can list results only from a certain site or blog. 
 * You can list only spam-free (approved blogs) results.
 * You can limit results from a certain timespan.  

All these settings and more are available per block.

You can read more about the options at http://www.twingly.com/help 

Twingly Blogrank
================
You also have a block that show your Twingly Blogrank. 

Ping Twingly with your content
============================== 
Every time cron runs Twingly module checks for new nodes of certain selected types. 
If there are any new matching nodes Twingly is pinged using xmlrpc. You find
the settings page at admin/settings/twingly.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Block settings on each Twingly block.

* Administer Twingly settings in Administer >> Settings >> Twingly.
  There you can pick your nodetypes that should be pinged to Twingly on creation. 

* Configure user permissions in Administer >> User management >> Permissions >>
  twingly module:

  - administer twingly
    Users in roles with the "administer twingly" permission will see
    be able to administer the settings at admin/settings/twingly.


-- FAQ --

Q:  I can’t find my blog posts on Twingly!
A:  Sorry, our bad. To remedy this: ping Twingly and make sure you’re reasonably 
    popular among your friends. Also make sure that your RSS feed validates 
    properly. Are you spammer? Then you won’t find your blog posts there. 


-- LINKS --
http://www.twingly.com/faq
http://www.twingly.com/help