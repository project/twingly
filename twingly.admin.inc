<?php

/**
 * @file
 * Admin page callbacks for the twingly module.
 */

/**
 * Returns the 'list' $op info for hook_block().
 */
function _twingly_block_list() {
  $blocks = array();

  $result = db_query('SELECT * FROM {twingly_block}');
  while ($block = db_fetch_object($result)) {
    $blocks[$block->bid]['info'] = 'Twingly: '.$block->searchphrase;
  }
  
  return $blocks;
}

/**
 * Displays the block configuration form.
 */
function twingly_admin_configure($delta = 0) {
  $block = twingly_block_data($delta);

  $form['searchphrase'] = array(
    '#type' => 'textfield',
    '#title' => t('Searchphrase'),
    '#maxlength' => 64,
    '#description' => '
    "site:domain.com" gives you only results from domain.com.<br/>
    "link:domain.com" gives you only results that links to domain.com.<br/>
    "tag:drupal" gives you only results tagged with drupal.<br/>
    You can also combine this with a search word ex. "link:domain.com drupal" gives you posts that contains drupal from domain.com.<br/>
    <a href="http://www.twingly.com/help">More info available here</a>
    ',
    '#required' => TRUE,
    '#default_value' => $block['searchphrase'],
  );
  $form['twinglytitle'] = array(
    '#type' => 'textfield',
    '#title' => t('Searchtitle'),
    '#maxlength' => 64,
    '#description' => 'Title that is displayed over the search results instead of standard Twingly Widget.',
    '#default_value' => $block['data']['twinglytitle'],
  );
  $form['search'] = array(
    '#type' => 'select',
    '#title' => t('Search'),
    '#description' => 'Normal search = Blogs, Spam-free search = Approved Blogs, Microblog = Results from Twitter etc.',
    '#options' => array('normal' => 'Normal search', 'spamfree' => 'Spam-free search (beta)', 'microblog' => 'Microblog'),
    '#default_value' => $block['data']['search'],
  );
  $form['count'] = array(
    '#type' => 'select',
    '#title' => t('Count'),
    '#description' => 'Number of results to show.',
    '#options' => array(1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10),
    '#default_value' => isset($block['data']['count']) ? $block['data']['count'] : 5,
  );
  $form['sort'] = array(
    '#type' => 'select',
    '#title' => t('Sort'),
    '#description' => '',
    '#options' => array('published' => t('Published'), 'inlinks' => t('Inlinks'), 'twingly' => t('Twingly')),
    '#default_value' => $block['data']['sort'],
  );

  $lang = array(
    '0' => t('All languages'),
    'sq' => 'Albanian',
    'ar' => 'Arabic',
    'bg' => 'Bulgarian',
    'hr' => 'Croatian',
    'cs' => 'Czech',
    'da' => 'Danish',
    'nl' => 'Dutch',
    'en' => 'English',
    'et' => 'Estonian',
    'fi' => 'Finnish',
    'fr' => 'French',
    'de' => 'German',
    'el' => 'Greek',
    'hu' => 'Hungarian',
    'is' => 'Icelandic',
    'it' => 'Italian',
    'lt' => 'Lithuanian',
    'mk' => 'Macedonian',
    'no' => 'Norwegian',
    'pl' => 'Polish',
    'pt' => 'Portuguese',
    'ro' => 'Romanian',
    'ru' => 'Russian',
    'sr' => 'Serbian',
    'sk' => 'Slovak',
    'sl' => 'Slovenian',
    'es' => 'Spanish',
    'sv' => 'Swedish',
    'tr' => 'Turkish',
    'uk' => 'Ukrainian',
  );
  
  $form['lang'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#description' => 'Only show posts in these languages.',
    '#options' => $lang,
    '#multiple' => TRUE,
    '#default_value' => isset($block['data']['lang']) ? $block['data']['lang'] : 0,
  );
  
  $tspan = array(
    '0'   => t('Anytime'),
    'h'   => t('Last hour'),
    '2h'  => t('Last 12 hours'),
    '24h' => t('Last 24 hours'),
    'w'   => t('Last week'),
    'm'   => t('Last month'),
  );
  
  $form['tspan'] = array(
    '#type' => 'select',
    '#title' => t('Timespan'),
    '#description' => 'Show result from certain timespan',
    '#options' => $tspan,
    '#default_value' => isset($block['data']['tspan']) ? $block['data']['tspan'] : '0',
  );

  $form['showblog'] = array(
    '#type' => 'select',
    '#title' => t('Show bloglink & RSS'),
    '#description' => 'Shows an RSS-icon and a link to the blog where the result comes from.',
    '#options' => array('No' => 'No', 'Yes' => 'Yes'),
    '#default_value' => $block['data']['showblog'],
  );
  
  return $form;
}

/**
 * Returns the 'save' $op info for hook_block().
 */
function _twingly_block_save($delta, $edit) {
  $data = array(
   'sort'     => $edit['sort'],
   'count'    => $edit['count'],
   'showblog' => $edit['showblog'],
   'search'   => $edit['search'],
   'lang'     => $edit['lang'],
   'twinglytitle'     => $edit['twinglytitle'],
   'tspan'     => $edit['tspan'],
  );

  $result = db_query('UPDATE {twingly_block} SET searchphrase = "%s",data="%s" WHERE bid = "%d"', $edit['searchphrase'], serialize($data), $delta);
}

/**
 * Menu callback: display the twingly block addition form.
 */
function twingly_add_block_form(&$form_state) {
  include_once(drupal_get_path('module', 'block').'/block.admin.inc');
  return block_admin_configure(&$form_state, 'twingly', 'NULL');
}

/**
 * Save the new twingly block.
 */
function twingly_add_block_form_submit($form, &$form_state) {
  $data = array(
   'sort'     => $form_state['values']['sort'],
   'count'    => $form_state['values']['count'],
   'showblog' => $form_state['values']['showblog'],
  );

  db_query('INSERT INTO {twingly_block} (searchphrase,data) VALUES ("%s","%s")', $form_state['values']['searchphrase'], serialize($data));
  $delta = db_last_insert_id('twingly_block', 'bid');

  foreach (list_themes() as $key => $theme) {
    if ($theme->status) {
      db_query("INSERT INTO {blocks} (visibility, pages, custom, title, module, theme, status, weight, delta, cache) VALUES(%d, '%s', %d, '%s', '%s', '%s', %d, %d, '%s', %d)", $form_state['values']['visibility'], trim($form_state['values']['pages']), $form_state['values']['custom'], $form_state['values']['title'], $form_state['values']['module'], $theme->name, 0, 0, $delta, BLOCK_NO_CACHE);
    }
  }

  foreach (array_filter($form_state['values']['roles']) as $rid) {
    db_query("INSERT INTO {blocks_roles} (rid, module, delta) VALUES (%d, '%s', '%s')", $rid, $form_state['values']['module'], $delta);
  }

  drupal_set_message(t('The twinglyblock has been created.'));
  cache_clear_all();

  $form_state['redirect'] = 'admin/build/block';
  return;
}

/**
 * Twingly settings form
 * @return unknown_type
 */
function twingly_settings_form() {
  $form = array(); 
  
  $form['twingly_ping_settings'] = array(
    '#type'  => 'fieldset',
    '#title' => t('General settings'),
  );

 foreach (node_get_types('types') as $index => $nodetype) {
    $nodetypes[$index] = $nodetype->name; 
 }
  $form['twingly_ping_settings']['twingly_ping_nodetypes'] = array(
    '#type'   => 'checkboxes',
    '#title'  => t('Ping Twingly on creation'),
    '#options' => $nodetypes, 
    '#default_value' => variable_get('twingly_ping_nodetypes', array()),
    '#description'   => t('Notify Twingly on creation of these nodetypes. They will be sent using cron. Twingly is a blog searchengine so you should only select bloglike nodetypes. Uncheck all types to disable this functionality.'),
  );
  
  return system_settings_form($form);
}